# voyager_regression

This repo contains the datasets and configuration files for testing the different strategies in phaser_voyager

The DATA folder contains the data for the given structures. 

The rest of the folders are named as the strategies/programs they aim to test. Inside them, the name of the structure tested is the next
level of folder, and inside it the phil file to run that strategy is found.

Update Claudia 13 Jan 2022: added an old_scripts folder to keep track of examples for strategies that are at the moment discontinued
