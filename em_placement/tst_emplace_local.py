from __future__ import print_function
from __future__ import division
import math
from libtbx.utils import null_out
from iotbx.data_manager import DataManager
from iotbx.map_model_manager import map_model_manager
from cctbx import adptbx
from scitbx.array_family import flex
from libtbx.utils import format_cpu_times
from libtbx.test_utils import approx_equal
import scitbx.rigid_body
import scitbx.matrix
import os,sys
import random

def perturb_model(model, shake=None, translate=None, rotate=None,
    random_seed=None, log=null_out()):
  # Perturb model by various combinations of shake, translate, rotate
  # For rotation, use zyz Euler angle convention
  # Adapted from create_models_or_maps.shake_model
  from mmtbx.pdbtools import master_params
  params = master_params().extract()
  if shake is not None:
    params.modify.sites[0].shake=shake
  if translate is not None:
    params.modify.sites[0].translate = translate
  if rotate is not None:
    params.modify.sites[0].rotate = rotate
    params.modify.sites[0].euler_angle_convention = "zyz"
  if random_seed is not None:
    params.modify.random_seed=random_seed
  from mmtbx.pdbtools import modify
  new_model = modify(
      model  = model,
      params = params.modify,
      log    = log).get_results().model
  return new_model

def exercise():
  """Test emplace_local using data and model with defined errors."""

  # Generate two half-maps with same anisotropic signal, independent anisotropic
  # noise, based on calculated structure factors from a model. Reorient the model
  # and try to place it in the map.

  # Use reasonably spherical model placed in the centre of a cubic unit cell
  # twice the maximum extent of the model, to simulate the situation with a
  # typical cryo-EM map.

  # Set flag for whether to print out debugging information instead of carrying
  # out regression tests with assert statements.
  # Should be set as False for released version!
  debug = False

  # Path-setting method here could be fragile and should be improved.
  module_path = os.path.dirname(os.path.abspath(__file__))
  src = os.path.join(module_path, "../../phaser_voyager/src/")
  sys.path.append(src)
  import libtbx.load_env
  iotbx_regression = os.path.join(libtbx.env.find_in_repositories("iotbx"),
      'regression')
  file_name=os.path.join(iotbx_regression,'data', 'big_cube_model.pdb')

  d_min = 5.5
  dm = DataManager()
  start_model = dm.get_model(file_name)

  # Reset B-values from zero to chosen constant
  b_iso = 30.
  b_values=flex.double(start_model.get_sites_cart().size(), b_iso)
  ph = start_model.get_hierarchy()
  ph.atoms().set_b(b_values)

  mmm = map_model_manager()
  mmm.generate_map(
      model=start_model,
      d_min=d_min, k_sol=0.1, b_sol=50.)

  # Turn starting map into map coeffs for the signal
  unit_cell = mmm.map_manager().unit_cell()
  ucpars = unit_cell.parameters()
  d_max=max(ucpars[0], ucpars[1], ucpars[2])
  start_map_coeffs = mmm.map_as_fourier_coefficients(d_min=d_min, d_max=d_max)

  # Apply anisotropic scaling to map coeffs
  b_target = (50.,100.,150.,-25.,25.,50.)
  u_star_s = adptbx.u_cart_as_u_star(
      unit_cell, adptbx.b_as_u(b_target))
  scaled_map_coeffs = start_map_coeffs.apply_debye_waller_factors(u_star=u_star_s)

  # Generate map coefficient errors for first half-map from complex normal
  # distribution
  b_target_e = (0.,0.,0.,-50.,-50.,50.) # Anisotropy for error terms
  u_star_e = adptbx.u_cart_as_u_star(
      unit_cell, adptbx.b_as_u(b_target_e))
  se_target = 1000000. # Target for SigmaE variance term
  rsigma = math.sqrt(se_target / 2.)
  jj = 0.+1.j  # Define I for generating complex numbers
  random_complexes1 = flex.complex_double()
  ncoeffs=start_map_coeffs.size()
  random.seed(123457) # Make runs reproducible
  for i in range(ncoeffs):
    random_complexes1.append(random.gauss(0.,rsigma) + random.gauss(0.,rsigma)*jj)
  rc1_miller = start_map_coeffs.customized_copy(data=random_complexes1)
  mc1_delta = rc1_miller.apply_debye_waller_factors(u_star=u_star_e)
  map1_coeffs = scaled_map_coeffs.customized_copy(
    data=scaled_map_coeffs.data() + mc1_delta.data())

  # Repeat for second half map with independent errors from same distribution
  random_complexes2 = flex.complex_double()
  for i in range(ncoeffs):
    random_complexes2.append(random.gauss(0.,rsigma) + random.gauss(0.,rsigma)*jj)
  rc2_miller = start_map_coeffs.customized_copy(data=random_complexes2)
  mc2_delta = rc2_miller.apply_debye_waller_factors(u_star=u_star_e)
  map2_coeffs = scaled_map_coeffs.customized_copy(
    data=scaled_map_coeffs.data() + mc2_delta.data())

  mmm.add_map_from_fourier_coefficients(
      map1_coeffs, map_id = 'map_manager_1')
  mmm.add_map_from_fourier_coefficients(
      map2_coeffs, map_id = 'map_manager_2')
  # Replace original map_manager with mean of half-maps
  mm_mean_data = (mmm.map_manager_1().map_data() + mmm.map_manager_2().map_data()) / 2
  mmm.map_manager().set_map_data(map_data = mm_mean_data)

  # Perturb starting model by combination of shaking, translating and rotating
  # to get search model. Specify random seed to make this reproducible.
  eulers = [25.,35.,55.]
  tvec = (10.,20.,30.)
  c_of_m_ori = start_model.get_sites_cart().mean()
  search_model = perturb_model(start_model, shake=1.0, translate=tvec,
      rotate=tuple(eulers), random_seed=123457)

  # Rotation to be applied to search model
  target_rotmat = scitbx.rigid_body.rb_mat_zyz(
          phi = -eulers[2],
          psi = -eulers[1],
          the = -eulers[0]).rot_mat().as_mat3() # Inverse of rotation applied to target

  # Translation to be applied to rotated search model
  vec3_c_of_m = flex.vec3_double([c_of_m_ori])
  new_c_of_m = flex.vec3_double([tvec]) + vec3_c_of_m
  target_trans = tuple(vec3_c_of_m - target_rotmat*(new_c_of_m))
  target_trans = flex.double(target_trans)

  # Write out (temporary) files needed by em_place_search_sphere
  import tempfile
  # create a temporary directory
  tmpdirname = tempfile.mkdtemp()
  if debug:
    print('\nCreated temporary directory', tmpdirname)
  map_filename = None
  map1_filename = os.path.join(tmpdirname, "map1.map" )
  map2_filename = os.path.join(tmpdirname, "map2.map" )
  model_filenameroot = os.path.join(tmpdirname, "model" )

  mmm.map_manager_1().write_map(file_name=map1_filename)
  mmm.map_manager_2().write_map(file_name=map2_filename)
  dm.set_overwrite(True)
  model_filename = dm.write_model_file(search_model, model_filenameroot)

  model_vrms = 1.2
  sphere_center = (ucpars[0]/2, ucpars[1]/2, ucpars[2]/2)
  sequence_composition = None
  fixed_scattering_ratio = 1.5
  top_files = 5
  refine_scale = True
  if debug:
    level = "logfile"
  else:
    level = "process"

  from New_Voyager.scripts.em_placement_script import emplace_local

  results = emplace_local(
      map_filename, map1_filename, map2_filename,
      d_min, model_filename, model_vrms,
      sphere_center=sphere_center,
      top_files = top_files,
      sequence_composition = sequence_composition,
      fixed_scattering_ratio = fixed_scattering_ratio,
      refine_scale = refine_scale,
      level = level)

  # Clean up temporary files
  import shutil
  shutil.rmtree(tmpdirname)

  # Now regression tests
  mapLLG_target = 600.
  mapLLG_eps = 30.
  mapCC_target = 0.66
  mapCC_eps = 0.04
  cell_scale_target = 1.
  cell_scale_eps = 0.005
  delta_rot_target = 0.
  delta_rot_eps = 1.
  delta_tra_target = 0.
  delta_tra_eps = 1.
  c_of_m_target = 0.
  c_of_m_eps = 0.5
  shift_cart = flex.double(results.top_models[0]._shift_cart) # set in focused docking
  c_of_m_final = flex.double(results.top_models[0].get_sites_cart().mean()) - shift_cart
  c_of_m_ori = flex.double(c_of_m_ori)
  delta_c_of_m = math.sqrt(flex.sum(flex.pow2(c_of_m_ori-c_of_m_final)))
  # Get rotation and translation to compare to ideal values
  top_model_RT = results.top_models_RT[0]
  top_model_rotmat = []
  top_model_trans = []
  for row in top_model_RT:
    top_model_rotmat.extend(row[0:3])
    top_model_trans.extend(row[3:])
  top_model_trans = flex.double(top_model_trans)
  delta_tra = math.sqrt(flex.sum(flex.pow2(target_trans-top_model_trans)))
  target_matrix = scitbx.matrix.sqr(target_rotmat)
  placed_matrix_transpose = scitbx.matrix.sqr(top_model_rotmat).transpose()
  delta_matrix = target_matrix*placed_matrix_transpose
  delta_rot = delta_matrix.rotation_angle(eps=1.e-4)

  if debug:
    print("\n\nRegression tests:\n")
    print("nsolfound: ", results.nsolfound)
    print("  target is a single solution")
    print("mapLLG: ",results.top_models_mapLLG[0])
    print("  target and allowed deviation: ", mapLLG_target, mapLLG_eps)
    print("mapCC: ",results.top_models_mapCC[0])
    print("  target and allowed deviation: ", mapCC_target, mapCC_eps)
    print("Cell scale: ",results.top_models_cell_scale[0])
    print("  target and allowed deviation: ",cell_scale_target, cell_scale_eps)
    print("rotation matrix: ",top_model_rotmat)
    print("  target: ",target_rotmat)
    print("rotation error: ", delta_rot)
    print("  target and allowed deviation: ", delta_rot_target, delta_rot_eps)
    print("translation vector error: ",delta_tra)
    print("  target and allowed deviation: ", delta_tra_target, delta_tra_eps)
    print("center of mass error: ", delta_c_of_m)
    print("  target and allowed deviation: ", c_of_m_target, c_of_m_eps)
  else:
    assert (results.nsolfound == 1)
    assert approx_equal(results.top_models_mapLLG[0], mapLLG_target, eps=mapLLG_eps)
    assert approx_equal(results.top_models_mapCC[0], mapCC_target, eps=mapCC_eps)
    assert approx_equal(results.top_models_cell_scale[0], cell_scale_target,
                        eps=cell_scale_eps)
    assert approx_equal(delta_rot, delta_rot_target, eps=delta_rot_eps)
    assert approx_equal(delta_tra, delta_tra_target, eps=delta_tra_eps)
    assert approx_equal(delta_c_of_m, c_of_m_target, eps=c_of_m_eps)

if(__name__ == "__main__"):
  exercise()
  print(format_cpu_times())
  print("OK")
