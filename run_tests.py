from libtbx import test_utils
import libtbx.load_env
import os.path

tst_list = (
  "$D/CASP_Rel_eLLG/Q9H790/tst_casp_rel_ellg_Q9H790.py",
  "$D/CASP_Rel_eLLG/T1024-D1/tst_casp_rel_ellg_1024.py",
  "$D/CASP_Rel_eLLG/R1030-D2/tst_casp_rel_ellg_1030.py",
  "$D/CASP_Rel_eLLG/T1038-D2/tst_casp_rel_ellg_1038.py",
  #    phenix.voyager.homologs is not currently distributed
  #"$D/Homologs/1qiv/tst_homologs_1qiv_noalign.py",
  #"$D/Homologs/1qiv/tst_homologs_1qiv_hhpred.py",
  # This one is also relatively long because it uses a local list of files
  #"$D/Homologs/1qiv/tst_homologs_1qiv_localfiles.py",
  # Skip Isostructure tests because this functionality is replaced by phasertng scripts
  #"$D/Isostructure/1qiv/tst_1qiv_isostructure.py",
  #"$D/Isostructure/2f6f/tst_2f6f_isostructure.py",
  #"$D/Isostructure/2trh/tst_2trh_isostructure.py",
  #    phenix.voyager.sculptor_ensembler is not currently distributed
  #"$D/Sculptor_Ensembler/beta_blip/tst_betablip_sculptor_ensembler.py",
  #    phenix.voyager.xtricorder is not currently distributed
  #"$D/Xtricorder/2cc0/tst_2cc0_xtricorder.py",
  #"$D/Xtricorder/2pan/tst_2pan_xtricorder.py",
  #"$D/Xtricorder/2trh/tst_2trh_xtricorder.py",
  "$D/em_placement/tst_emplace_local.py",
  "$D/em_placement/tst_em_placement.py",
  # Xtricorder functionality replaced by phasertng.xtricorder
  #"$D/Xtricorder/test_xtricorder.py",

  )

def run():

  dist_dir = os.path.join(libtbx.env.under_dist("voyager_regression", ""))
  build_dir = ""

  test_utils.run_tests(build_dir, dist_dir, tst_list)


if (__name__ == "__main__"):
  run()
