
from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path, shutil
from pathlib import PurePath
from phaser_regression import additional_regressions


def exercise(dirty=False) :
  dpath = os.path.join(libtbx.env.under_dist("voyager_regression", ""), "Xtricorder")
  tmppath = PurePath(os.path.join(os.getcwd(), "xtricorder_tmp")).as_posix()
  mtzfname = os.path.join(dpath, "3P56.mtz")

  strargs = 'phasertng.xtricorder phasertng.hklin.filename="%s" phasertng.suite.database="%s"'  %(mtzfname, tmppath)
  result = easy_run.fully_buffered(strargs)
  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  #if not dirty:
  #  shutil.rmtree(tmppath)

if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
