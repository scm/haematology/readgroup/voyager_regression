from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path, time, shutil

cwd = os.getcwd()

def exercise(dirty=False) :
  dpath = libtbx.env.under_dist("voyager_regression", "Isostructure/2f6f/")
  namephil = '2f6f_isostructure_correct.phil'
  philpath = os.path.join(cwd, namephil)
  fichiphil = open(philpath, 'w')
  fichiphil.write("""voyager{
  reflections = """+os.path.join(dpath,'../../DATA/2f6f/2f6f-sf.mtz')+"""
  level = verbose
  remove_phasertng_folder = True
  isostructure_search.reindex_as = """+os.path.join(dpath,'../../DATA/2f6f/2f6z-sf.mtz')+"""
    }""")
  del fichiphil


  strargs = "phenix.voyager.isostructure "+philpath

  start = time.time()
  result = easy_run.fully_buffered(strargs)
  done = time.time()
  #result.show_stdout()
  elapsed = done - start
  path_output = os.path.join(cwd,'ISOSTRUCTURE_folder')
  if os.path.exists(path_output):
    print('The output ISOSTRUCTURE_folder was generated')
    print('And it contains the file ',os.listdir(path_output))
  print("Total time elapsed is", elapsed)

  if not result.return_code == 0:
    nlines = 50
    for line in result.stdout_lines[-nlines:]:
      print(line)
    for line in result.stderr_lines[-nlines:]:
      print(line)
    print("Runtime error " + str(result.return_code))
  assert result.return_code == 0

  # tidy up
  if not dirty:
    tmpfiles = [namephil,'voyager.log']
    tmpfolders = ['ISOSTRUCTURE_folder']
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass
    for d in tmpfolders:
      try:
        shutil.rmtree(d)
      except Exception as e:
        pass



if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
