from __future__ import print_function
from __future__ import division

from libtbx import easy_run
import libtbx.load_env
import sys, os, os.path, time

cwd = os.getcwd()
module_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(module_path, "../")) # Find compareCSVs

def exercise(dirty=False) :
  dpath = libtbx.env.under_dist("voyager_regression","CASP_Rel_eLLG/Q9H790")
  # we will not use the phil file but rather we will write one with the required content

  namephil = 'Q9H790_correctpath.phil'
  name_csv = "Q9H790_table_rellg.csv"
  csvpath = os.path.join(cwd, name_csv)
  philpath = os.path.join(cwd, namephil)
  fichiphil = open(philpath, 'w')
  fichiphil.write("""voyager
  {
      level = logfile

      generate_dataset
      {
          target_model = """ + os.path.join(dpath, 'targets/7lw7A.pdb') + """\n
          resolution_dataset = 2.0
      }

      prepare_casp_models_for_evaluation
      {
          models = """ + os.path.join(dpath, 'Q9H790_AFmodels') + """\n
      }

      models_evaluation_against_target
      {
          bfactor_treatment = all
      }

      analysis_casp_evaluation
      {
          output_csv_filename = """ + csvpath + """\n
      }
  }""")
  del fichiphil

  strargs = "phenix.voyager.casp_rel_ellg "+philpath

  debug = False
  start = time.time()
  result = easy_run.fully_buffered(strargs)
  done = time.time()
  elapsed = done - start

  assert os.path.exists(csvpath)
  checkcsv = os.path.join(dpath, 'check.csv')
  # import compare_CSVs
  from compare_CSVs import compare_csv_files_with_dynamic_threshold
  ndiff = compare_csv_files_with_dynamic_threshold(checkcsv, csvpath, fraction=0.2, epsilon=0.2)
  if ndiff or debug:
    if ndiff:
      print("ERROR: output CSV differs significantly from target")
    print("**** Output CSV ****")
    with open(csvpath, 'r') as f:
      print(f.read())
    print("**** Target CSV ****")
    with open(checkcsv, 'r') as f:
      print(f.read())

  if debug:
    print("Total time elapsed is", elapsed,'\n')
  assert ndiff == 0

  # tidy up
  if not dirty:
    tmpfiles = [name_csv,namephil]
    for f in tmpfiles:
      try:
        os.remove(f)
      except Exception as e:
        pass


if (__name__ == "__main__"):
  if len(sys.argv) > 1:
    exercise(sys.argv[1] == "dirty")
  else:
    exercise()
  print("OK")
