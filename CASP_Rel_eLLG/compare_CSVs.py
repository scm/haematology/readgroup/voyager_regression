import pandas as pd

def compare_csv_files_with_dynamic_threshold(file1, file2, fraction, epsilon):
    # Read the CSV files into DataFrames
    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)

    # Check if both DataFrames have the same structure (columns)
    if list(df1.columns) != list(df2.columns):
        print("Error: CSV files do not have the same structure (different columns).")
        return

    # Select only numeric columns to avoid comparison errors
    numeric_cols = df1.select_dtypes(include=['number']).columns

    # Calculate absolute differences between the two DataFrames for numeric columns
    diff = (df1[numeric_cols] - df2[numeric_cols]).abs()

    # Calculate the dynamic threshold for each value in df1
    dynamic_threshold = df1[numeric_cols].abs() * fraction + epsilon

    # Find rows where any absolute difference exceeds the dynamic threshold
    differences = diff[(diff > dynamic_threshold).any(axis=1)]

    # Combine with non-numeric columns to get a complete view of the rows with differences for debugging
    # result = pd.concat([df1.loc[differences.index], df2.loc[differences.index], differences], axis=1, keys=['File1', 'File2', 'Difference'])
    # print(result)

    return differences.size
